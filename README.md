# meltano-charm

A publicly available juju charm which instantiates meltano along with a boilerplate meltano project

# Quickstart

It's expected you're setting this up as part of a bundle.. but if you want to just launch it

```
juju deploy meltano
```

In general, you need to pass the required strings in that bundle like so

# WARNING, NEVER GIT COMMIT YOUR PRIVATE KEYS.  USE READ ONLY DEPLOY KEYS FOR `github_private_key`

```
  meltano:
    charm: meltano
    num_units: 1
    to:
    - "1"
    options:
      git_private_key_proj: |
        -----YOUR RSA PRIVATE KEY IF PROJECT IS PRIVATE-----
      git_repo_proj: https://gitlab.com/jrgemcp-public/meltano-project-starter.git
```

